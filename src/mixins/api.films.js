export default {
    methods: {
        async getFilms(type = 'upcoming', page = 1, with_genres = 28) {
            const response = await fetch("https://api.themoviedb.org/3/movie/" + type + "?language=fr-FR&api_key=a5fa556963902c2c8c768941f0e54f3c&page=" + page + "&with_genres=" + with_genres);
            const data = await response.json();
            return data.results;
        },
        async getFilm(idFilm) {
            const response = await fetch("https://api.themoviedb.org/3/movie/" + idFilm + "?language=fr-FR&api_key=a5fa556963902c2c8c768941f0e54f3c");
            const data = await response.json();
            return data;
        },
        async getRandomFilm() {
            let data = {};
            data.success = false;
            while(typeof data.id === "undefined" && !data.success)
            {
                const response = await fetch("https://api.themoviedb.org/3/movie/" + Math.trunc(Math.random() * 100000) + "?language=fr-FR&api_key=a5fa556963902c2c8c768941f0e54f3c");
                data = await response.json();
            }
            return data;
        },
        async searchFilms(filtres = []) {
            let fetchString = "https://api.themoviedb.org/3/search/movie?language=fr-FR&api_key=a5fa556963902c2c8c768941f0e54f3c";
            for (const index in filtres) {
                if(filtres[index] != '')
                    fetchString += `&${index}=${filtres[index]}`
            }
            const response = await fetch(fetchString);
            const data = await response.json();
            return data.results;
        },
        async getGenres() {
            const response = await fetch("https://api.themoviedb.org/3/genre/movie/list?api_key=a5fa556963902c2c8c768941f0e54f3c&language=fr-Fr");
            const data = await response.json();
            console.log(data)
            console.log(data.genre)
            return data.genres;
        }
    }
}
export default {
    methods: {
        async addWishlist(film) {
            let wishlist = JSON.parse(localStorage.getItem('Wishlist')) || {};
            wishlist[film.id] = film
            localStorage.setItem('Wishlist', JSON.stringify(wishlist))
        },
        async removeWishlist(film) {
            let wishlist = JSON.parse(localStorage.getItem('Wishlist')) || {};
            delete wishlist[film.id]
            localStorage.setItem('Wishlist', JSON.stringify(wishlist??{}))
        },
        async emptyWishlist() {
            localStorage.setItem('Wishlist', JSON.stringify({}))
        },
    }
}
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Contact from "../views/Contact";
import Films from "../views/FilmsPage";
import Film from "../views/FilmPage";
import FilmSearch from "../views/FilmSearch";
import Wishlist from "../views/Wishlist";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/films',
    name: 'Films',
    component: Films
  },
  {
    path: '/films/:type',
    name: 'Films',
    component: Films
  },
  {
    path: '/search',
    name: 'Search',
    component: FilmSearch
  },
  {
    path: '/films/id/:id',
    name: 'Film',
    component: Film
  },
  {
    path: '/wishlist',
    name: 'Wishlist',
    component: Wishlist
  },
]

const router = new VueRouter({
  routes
})

export default router
